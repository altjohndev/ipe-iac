provider "aws" {
  region = var.region

  default_tags {
    tags = {
      Context     = "ipe"
      Environment = var.environment
      SourceRepo  = "https://gitlab.com/altjohndev/ipe-iac"
      AppRepo     = "https://gitlab.com/altjohndev/ipe-api"
      ManagedBy   = "terraform"
    }
  }
}

resource "aws_instance" "this" {
  ami           = var.ami
  instance_type = var.instance_type

  associate_public_ip_address = true

  key_name = var.ssh_key_name

  user_data = <<-EOF
  #!/bin/bash

  yum update -y
  yum install -y docker git
  service docker start
  chkconfig docker on

  curl \
    -L https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m) \
    -o /usr/local/bin/docker-compose

  chmod +x /usr/local/bin/docker-compose

  mkdir /opt/services

  cd /opt/services

  cat > docker-compose.yml << EOL
  version: "3"

  services:
    api:
      image: altjohndev/ipe-api:v0.0.1
      environment:
        IPE_API_POSTGRES_URL: ecto://postgres:postgres@postgres:5432/ipe
      ports:
        - 80:80
      depends_on:
        - postgres

    postgres:
      image: postgres:16.0
      environment:
        POSTGRES_DB: ipe
        POSTGRES_PASSWORD: postgres
  EOL

  docker-compose up -d
  EOF

  user_data_replace_on_change = true

  tags = {
    Name = var.instance_name
  }
}
