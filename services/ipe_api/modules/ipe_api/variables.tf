variable "ami" {}
variable "environment" {}
variable "instance_name" {}
variable "instance_type" {}
variable "region" {}
variable "ssh_key_name" {}
