include "root" {
  path = find_in_parent_folders()
}

terraform {
  source = "modules//ipe_api"
}

inputs = {
  ami           = "ami-065ab11fbd3d0323d"
  environment   = "staging"
  instance_name = "ipe_api"
  instance_type = "t2.micro"
  region        = "eu-central-1"
  ssh_key_name  = "ipe-admin"
}
