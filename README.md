# Ipê IaC

[Ipê](https://gitlab.com/altjohndev/ipe) Infrastructure as Code.

## Setup

With [asdf](https://asdf-vm.com/) installed, run:

```bash
asdf install
```

Make sure that you have proper
[aws credentials](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html) set.

## Usage

```bash
# Planning all services
terragrunt run-all plan --terragrunt-working-dir services
# Applying all services
terragrunt run-all apply --terragrunt-working-dir services
# Destroying all services
terragrunt run-all destroy --terragrunt-working-dir services
```
